from setuptools import setup, find_packages
import os

version = '0.1.3'

setup(name='datoscientificos.theme',
      version=version,
      description="Theme for datoscientificos.cl",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from
      # http://pypi.python.org/pypi?:action=list_classifiers
      classifiers=[
        "Framework :: Plone",
        "Programming Language :: Python",
        ],
      keywords='',
      author='Degu',
      author_email='contacto@degu.cl',
      url='http://degu.cl',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['datoscientificos'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          'plone.app.theming',
          'collective.carousel',
          #'collective.responsivetheme',
          #'collective.js.jqueryui',  # I don't know why This does't works.
      ],
      entry_points="""
      # -*- Entry points: -*-

      [z3c.autoinclude.plugin]
      target = plone
      """,
      setup_requires=["PasteScript"],
      paster_plugins=["ZopeSkel"],
      )
